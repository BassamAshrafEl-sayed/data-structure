package eg.edu.alexu.csd.datastructure.stack.cs16;

import java.util.Scanner;

public class StackMain {

	public static void main(String[] args) {
		StackImp x = new StackImp();
		 		int choice = 0;
		 		Scanner in = new Scanner(System.in);
		 		while(true)
		 		{
		 			x.printInterface();
		 			choice = x.choiceInput();
		 			if (choice == 1) {
		 				x.printPush();
		 				Object element = in.next();
		 				x.push(element);
		 				System.out.println("Element pushed successfully !");
		 				System.out.println("Enter any key to return");
		 				in.nextLine();
		 				in.nextLine();
		 			} else if (choice == 2) {
		 				try{
		 					x.printPop(x.pop());
		 					System.out.println("Element poped successfully !");
		 					System.out.println("Enter any key to return");
		 					in.nextLine();
		 				}catch (Exception e) {
		 					System.out.println("There is no element to be poped !");
		 					System.out.println("Enter any key to return");
		 					in.nextLine();
		 				}
		 			} else if (choice == 3) {
		 				try{
		 					x.printpeek(x.peek());
		 					System.out.println("Enter any key to return");
		 					in.nextLine();
		 				}catch (Exception e) {
		 					System.out.println("There is no element to be shown !");
		 					System.out.println("Enter any key to return");
		 					in.nextLine();
		 				}
		 			} else if (choice == 4) {
		 				x.printsize(x.size());
		 				System.out.println("Enter any key to return");
		 				in.nextLine();
		 			} else if (choice == 5) {
		 				x.printEmpty(x.isEmpty());
		 				System.out.println("Enter any key to return");
		 				in.nextLine();
		 			} else if (choice == 6) {
		 				break;
		 			}
		 		}

	}

}
