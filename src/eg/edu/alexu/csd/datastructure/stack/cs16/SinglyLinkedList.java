package eg.edu.alexu.csd.datastructure.stack.cs16;

import eg.edu.alexu.csd.datastructure.linkedList.ILinkedList;

public class SinglyLinkedList implements ILinkedList {
	public Node head = null;
	public int size = 0;

	/*
	 * Inserts a specified element at the specified sposition in the list.
	 */
	@Override
	public void add(final int index,final Object Value) {
		Node element = new Node();
		element.value = Value;
		if (index > size || index < 0) {
			throw null;
		} else if (index == 0) {
			element.next = head;
			head = element;
			size++;
		} else {
			Node temp = theElementBefore(index);
			element.next = temp.next;
			temp.next = element;
			size++;
		}
	}
	/* Inserts the specified element at the end of the list. */
	@Override
	public void add(final Object Value) {
		Node element = new Node();
		element.value = Value;
		if (size == 0) {
			head = element;
			head.next = null;
			size++;
		} else {
			Node temp = theElementBefore(size);
			temp.next = element;
			element.next = null;
			size++;
		}

	}
	/* Returns the element at the specified position in this list. */
	@Override
	public Object get(final int index) {
		Node temp = head;
		if (index < size && index >= 0) {
			for (int i = 0; i < index; i++) {
				temp = temp.next;
			}
		} else {
			throw null;
		}
		return temp.value;
	}
	/*
	* Replaces the element at the specified position in this list with
	* the specified element.
	*/
	@Override
	public void set(final int index,final Object Value) {
		Node element = new Node();
		element.value = Value;
		if (index < size && index != 0) {
			Node temp = theElementBefore(index);
			element.next = temp.next.next;
			temp.next = element;
		} else if (index == 0) {
			element.next = head.next;
			head = element;
		} else {
			throw null;
		}

	}
	/* Removes all of the elements from this list. */
	@Override
	public void clear() {
		if (size > 0) {
			head = null;// note here
			size = 0;
		} else {
			throw null;
		}
	}
	/* Returns true if this list contains no elements. */
	@Override
	public boolean isEmpty() {
		boolean Empty = false;
		if (size == 0)
			Empty = true;
		return Empty;
	}
	/* Removes the element at the specified position in this list. */
	@Override
	public void remove(final int index) {
		if (index >= size || index < 0) {// note here
			throw null;
		}
		if (index < size && size != 0 && index > 0) {
			Node temp = theElementBefore(index);
			Node removed = temp.next;
			temp.next = removed.next;
			size--;
		} else if (index == 0 && size != 0) {
			head = head.next;
			size--;
		}
	}
	/* Returns the number of elements in this list. */
	@Override
	public int size() {
		return size;
	}
	/*
	* Returns a view of the portion of this list between the specified
	* fromIndex and toIndex, inclusively.
	*/
	@Override
	public ILinkedList sublist(final int fromIndex,final int toIndex) {
		SinglyLinkedList list = new SinglyLinkedList();
		if (fromIndex < size && toIndex < size && fromIndex <= toIndex) {
			Node temp = theElementBefore(fromIndex);
			if (fromIndex > 0) {
				temp = temp.next;
			}
			for (int i = 0; i <= toIndex - fromIndex; i++) {

				list.addWithoutGrounding(temp.value);
				temp = temp.next;

			}
			temp.next = null;

		} else {
			throw null;
		}

		return list;
	}
	/*
	* Returns true if this list contains an element with the same value
	* as the specified element.
	*/
	@Override
	public boolean contains(final Object o) {
		Node temp = head;
		if (size == 0) {
			throw null;
		}
		for (int i = 0; i < size; i++) {
			if (o.equals(temp.value)) {
				return true;
			}
			temp = temp.next;
		}
		return false;
	}
	/*
	 * Add the element without grounding
	 * @param Value
	 */
	public void addWithoutGrounding(final Object Value) {
		Node element = new Node();
		element.value = Value;
		if (size == 0) {
			head = element;
			size++;
		} else {
			Node temp = theElementBefore(this.size);
			temp.next = element;
			size++;
		}
	}
	/*
	 * Get the element before the index
	 * @param index
	 * @return element before index
	 */
	public Node theElementBefore(final int index) {
		Node temp = head;
		for (int i = 0; i < index - 1; i++)
			temp = temp.next;
		return temp;
	}
	/*
	 * Print all the list
	 */
	void printList() {
		Node temp = head;
		for (int i = 0; i < size; i++) {
			System.out.print(this.get(i) + " ");
			temp = temp.next;
		}
	}
	public Object getTop(){
		return head.value;
	}
}
