package eg.edu.alexu.csd.datastructure.stack.cs16;

import eg.edu.alexu.csd.datastructure.stack.IExpressionEvaluator;

public class ExpressionImp implements IExpressionEvaluator {
	StackImp stack = new StackImp();
	StringBuilder build = new StringBuilder();
	int result = 0;

	// evaluate
	/*
	 * Takes a symbolic/numeric infix expression as input and converts it to
	 * postfix notation. There is no assumption on spaces between terms or the
	 * length of the term (e.g., two digits symbolic or numeric term)
	 * 
	 * @param expression infix expression
	 * 
	 * @return postfix expression
	 */
	@Override
	public String infixToPostfix(String expression) {
		scan(expression);
		if (stack.size() > 0) {
			while (true) {
				try {
					build((char) stack.pop());
				} catch (Exception e) {
					break;
				}
			}
		}
		build.deleteCharAt(build.length() - 1);
		String post = String.valueOf(build);
		return post;
	}

	/*
	 * Evaluate a postfix numeric expression, with a single space separator
	 * 
	 * @param expression postfix expression
	 * 
	 * @return the expression evaluated value
	 */
	@Override
	public int evaluate(String expression) {
		postfixScan(expression);
		result = Integer.parseInt((String) stack.pop());
		return result;
	}

	
	/*
	 * @param x is the expression
	 * the function scan every single character and do process according to the type
	 */
	public void scan(String x) {
		for (int i = 0; i < x.length(); i++) {
			if (x.charAt(i) == ' ' || x.charAt(i) == '	') {
				continue;
			}
			int type = checkType(x.charAt(i), i);
			if (type == 1) {
				build(x.charAt(i));
				if (i > 0) {
					if (x.charAt(i - 1) >= '0' && x.charAt(i - 1) <= '9') {
						build.deleteCharAt(build.length() - 3);
					}
				}
			} else if (type == 2) {
				expression(x.charAt(i), checkPrecedence(x.charAt(i)));
			} else if (type == 3) {
				if (x.charAt(i) == '(') {
					stack.push(x.charAt(i));
				} else {
					popTillOpen();
				}
			}
		}
	}


	/*
	 * @param x the char
	 * @param i the index of char
	 * @return the type of the char
	 */
	public int checkType(char x, int i) {
		x = Character.toUpperCase(x);
		if (i == 0) {
			if ((x >= '0' && x <= '9') || (x >= 'A' && x <= 'Z') || x == '(') {
				if (x == '(') {
					return 3;
				} else {
					return 1;
				}
			} else {
				throw null;
			}

		} else {
			if ((x >= '0' && x <= '9') || (x >= 'A' && x <= 'Z')) {
				return 1;
			} else if (x == '/' || x == '*' || x == '-' || x == '+') {
				return 2;
			} else if (x == '(' || x == ')') {
				return 3;
			} else {
				throw null;
			}
		}
	}

	
	/**
	 * @param x1 the char
	 * @return the precedence according to the last element in the stack
	 */
	public boolean checkPrecedence(char x1) {
		if (x1 == '/' || x1 == '*') {
			if (stack.size() == 0) {
				return true;
			} else if ((char) stack.peek() == '/' || (char) stack.peek() == '*') {
				return false;
			} else {
				return true;
			}
		} else if (x1 == '+' || x1 == '-') {
			if (stack.size() == 0) {
				return true;
			} else if ((char) stack.peek() == '/' || (char) stack.peek() == '*' || (char) stack.peek() == '-'
					|| (char) stack.peek() == '+') {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	
	/**
	 * @param x element in expression
	 * add the element to the new expression
	 */
	public void build(char x) {
		build.append(x);
		build.append(' ');
	}

	
	/**
	 * function build the postfix expression
	 * @param x the element in infix expression
	 * @param y check if the element is higher precedence or not
	 */
	public void expression(char x, boolean y) {
		if (y) {
			stack.push(x);
		} else {
			while (!checkPrecedence(x)) {
				if ((char) stack.peek() != '(') {
					build((char) stack.pop());
				}
			}
			stack.push(x);
		}
	}

	/*
	 * function pop all element from the stack till found an open bracket
	 */
	public void popTillOpen() {
		char pop = ' ';
		while (pop != '(') {
			pop = (char) stack.pop();
			if (pop != '(') {
				build(pop);
			}
			if (stack.size() == 0) {
				break;
			}
		}
	}

	
	/**
	 * @param x check if the char is operand
	 * @return true if operand , false otherwise
	 */
	public boolean isOperand(char x) {
		x = Character.toUpperCase(x);
		if ((x >= '0' && x <= '9') || (x >= 'A' && x <= 'Z'))
			return true;
		return false;
	}

	
	/**
	 * @param x check if the char is operator
	 * @return true if operator , false otherwise
	 */
	public boolean isOperator(char x) {
		if (x == '/' || x == '*' || x == '-' || x == '+')
			return true;
		return false;
	}

	
	/**
	 * @param x is postfix expression
	 * scan every single char in x and do some proccess according to the type
	 */
	public void postfixScan(String x) {
		for (int i = 0; i < x.length(); i++) {
			if (x.charAt(i) == ' ' || x.charAt(i) == '	') {
				continue;
			}
			if (isOperandP(x.charAt(i))) {
				if (i == 0) {
					stack.push(x.charAt(i));
				} else if (isOperandP(x.charAt(i))&&isOperandP(x.charAt(i-1))) {
					StringBuilder y = new StringBuilder();
					y.append(stack.pop());
					y.append(x.charAt(i));
					stack.push(y);	
				} else {
					stack.push(x.charAt(i));
				}
			} else if (isOperator(x.charAt(i))) {
				checkPostfix(x.charAt(i));
			} else {
				throw null;
			}
		}
	}

	
	/**
	 * @param x the element in postfix expression
	 * @return true if it was number and false otherwise
	 */
	public boolean isOperandP(char x) {
		if (x >= '0' && x <= '9')
			return true;
		return false;
	}

	
	/**
	 * @param x the element in postfix expression
	 *	check all element in postfix and convert them into integers
	 */
	public void checkPostfix(char x) {
		String c1 = String.valueOf(stack.pop());
		String c2 = String.valueOf(stack.pop());
		// String c2 = String.valueOf("5");
		if (x == '/') {
			int operand1 = Integer.parseInt(c1);
			int operand2 = Integer.parseInt(c2);
			stack.push(String.valueOf((int) (operand2 / operand1)));
		} else if (x == '*') {
			int operand1 = Integer.parseInt(c1);
			int operand2 = Integer.parseInt(c2);
			stack.push(String.valueOf((int) (operand1 * operand2)));
		} else if (x == '-') {
			int operand1 = Integer.parseInt(c1);
			int operand2 = Integer.parseInt(c2);
			stack.push(String.valueOf((int) (operand2 - operand1)));
		} else {
			int operand1 = Integer.parseInt(c1);
			int operand2 = Integer.parseInt(c2);
			stack.push(String.valueOf((int) (operand1 + operand2)));
		}
	}
}