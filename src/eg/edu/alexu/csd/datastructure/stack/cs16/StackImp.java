package eg.edu.alexu.csd.datastructure.stack.cs16;

import java.util.Scanner;

import eg.edu.alexu.csd.datastructure.stack.IStack;

public class StackImp implements IStack {
	SinglyLinkedList stack = new SinglyLinkedList();

	/*
	 * Inserts a specified element at the specified position in the list.
	 * 
	 * @param index zero-based index
	 * 
	 * @param element object to insert
	 */
	@Override
	public void add(int index, Object element) {
		stack.add(stack.size - index, element);
	}

	/*
	 * Removes the element at the top of stack and returns that element.
	 * 
	 * @return top of stack element, or through exception if empty
	 */
	@Override
	public Object pop() {
		if (stack.isEmpty())
			throw null;
		Object temp = stack.getTop();
		stack.remove(0);
		return temp;

	}

	/*
	 * Get the element at the top of stack without removing it from stack.
	 * 
	 * @return top of stack element, or through exception if empty
	 */
	@Override
	public Object peek() {
		if (stack.isEmpty())
			throw null;
		Object temp = stack.getTop();
		return temp;
	}

	/*
	 * Tests if this stack is empty
	 * 
	 * @return true if stack empty
	 */
	@Override
	public void push(Object element) {
		stack.add(0, element);

	}

	/*
	 * Tests if this stack is empty
	 * 
	 * @return true if stack empty
	 */
	@Override
	public boolean isEmpty() {
		return stack.isEmpty();
	}

	/*
	 * Returns the number of elements in the stack.
	 * 
	 * @return number of elements in the stack
	 */
	@Override
	public int size() {
		return stack.size;
	}

	
	/*
	 * print interface
	 */
	public void printInterface() {
		System.out.println("--------------------------------------------------------------------------");
		System.out.println("|                                                                        |");
		System.out.println("|                                                                        |");
		System.out.println("|                                                                        |");
		System.out.println("|             	            1-Push                                       |");
		System.out.println("|                                                                        |");
		System.out.println("|             	            2-Pop                                        |");
		System.out.println("|                                                                        |");
		System.out.println("|             	            3-Peek                                       |");
		System.out.println("|                                                                        |");
		System.out.println("|                           4-Get size                                   |");
		System.out.println("|                                                                        |");
		System.out.println("|                           5-Check empty                                |");
		System.out.println("|                                                                        |");
		System.out.println("|                           6-Exit                                       |");
		System.out.println("|                                                                        |");
		System.out.println("|                                                                        |");
		System.out.println("--------------------------------------------------------------------------");
		System.out.print("Your choice >> ");
	}
	/*
	 * verify input
	 */
	public int choiceInput() {
		String choice = new String();
		int result = 0;
		Scanner in = new Scanner(System.in);
		choice = in.nextLine();
		if (choice.length() != 1 || !(choice.charAt(0) > '0' && choice.charAt(0) <= '6')) {
			result = -1;
			return result;
		} else {
			result = Integer.parseInt(choice);
		}

		return result;
	}

	/*
	 * print push interface
	 */
	public void printPush() {
		System.out.println("--------------------------------------------------------------------------");
		System.out.println("|                                                                        |");
		System.out.println("|                                                                        |");
		System.out.println("|                      Enter element to push :                           |");
		System.out.println("|                                                                        |");
		System.out.println("|                                                                        |");
		System.out.println("--------------------------------------------------------------------------");
	}

	/*
	 * print popped element
	 */
	public void printPop(Object x) {
		System.out.println("--------------------------------------------------------------------------");
		System.out.println("|                                                                        |");
		System.out.println("|                                                                        |");
		System.out.println("|                    element poped : " + x + "                                   |");
		System.out.println("|                                                                        |");
		System.out.println("|                                                                        |");
		System.out.println("--------------------------------------------------------------------------");
	}

	/*
	 * print element at peek
	 */
	public void printpeek(Object x) {
		System.out.println("--------------------------------------------------------------------------");
		System.out.println("|                                                                        |");
		System.out.println("|                                                                        |");
		System.out.println("|                    element in peek : " + x + "                                 |");
		System.out.println("|                                                                        |");
		System.out.println("|                                                                        |");
		System.out.println("--------------------------------------------------------------------------");
	}

	/*
	 * 
	 */
	public void printsize(Object x) {
		System.out.println("--------------------------------------------------------------------------");
		System.out.println("|                                                                        |");
		System.out.println("|                                                                        |");
		System.out.println("|                             size : " + x + "                                   |");
		System.out.println("|                                                                        |");
		System.out.println("|                                                                        |");
		System.out.println("--------------------------------------------------------------------------");
	}

	/*
	 * print if stack is empty or not
	 */
	public void printEmpty(boolean x) {
		if (x) {
			System.out.println("--------------------------------------------------------------------------");
			System.out.println("|                                                                        |");
			System.out.println("|                                                                        |");
			System.out.println("|                         the stack is empty                             |");
			System.out.println("|                                                                        |");
			System.out.println("|                                                                        |");
			System.out.println("--------------------------------------------------------------------------");
		} else {
			System.out.println("--------------------------------------------------------------------------");
			System.out.println("|                                                                        |");
			System.out.println("|                                                                        |");
			System.out.println("|                         the stack isn't empty                          |");
			System.out.println("|                                                                        |");
			System.out.println("|                                                                        |");
			System.out.println("--------------------------------------------------------------------------");
		}
	}

}
