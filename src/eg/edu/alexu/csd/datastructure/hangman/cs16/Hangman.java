package eg.edu.alexu.csd.datastructure.hangman.cs16;

import java.util.Random;

import eg.edu.alexu.csd.datastructure.hangman.IHangman;

public class Hangman implements IHangman {
	private String[] dictionary;
	private String secretWord;
	private String dictionaryWord;
	private char originalWord[];
	private String editableWord;
	private int guesses = 0;
	private int maxGuesses;
	/*
	 * Set dictinary words to pick secret words from
	 * @param words an array of words
	 */
	@Override
	public void setDictionary(final String[] words) {
		this.dictionary = words;
	}
	/*
	* Pick a random secret word from dictionary and reutns it
	* @return secret word
	*/
	@Override
	public String selectRandomSecretWord() {
		if (this.dictionary == null) {
			return null;
		}
		Random Rn = new Random();
		this.dictionaryWord = getDictionary(Rn.nextInt(dictionary.length));
		this.secretWord = this.dictionaryWord.toUpperCase();
		originalWord = new char[this.secretWord.length()];
		for (int i = 0; i < this.secretWord.length(); i++) {
			this.originalWord[i] = '-';
		}
		return dictionaryWord;
	}
	/*
	* Receive a new user guess, and verify it against the secret word.
	* @param c
	* case insensitive user guess.
	* If c is NULL then ignore it and do no change
	* @return
	* secret word with hidden characters (use �-� instead unsolved
	* characters), or return NULL if user reached max wrong guesses
	*/
	@Override
	public String guess(final Character c) {
		if (c == null) {
			this.editableWord = String.valueOf(this.originalWord);
			return this.editableWord;
		} else {
			int state;
			state = 0;
			for (int i = 0; i < secretWord.length(); i++) {
				if (originalWord[i] == '-') {
					state = 1;
					break;
				}
			}
			if (state == 0) {
				return null;
			}
		}
		if (this.guesses < this.maxGuesses - 1) {
			char x = Character.toUpperCase(c);
			int flag = 0;
			for (int i = 0; i < this.secretWord.length(); i++) {
				if (this.secretWord.charAt(i) == x) {
					this.originalWord[i] = this.dictionaryWord.charAt(i);
					flag = 1;
				}
			}
			if (flag == 1) {
				this.editableWord = String.valueOf(this.originalWord);
				flag = 0;
			} else {
				this.editableWord = String.valueOf(this.originalWord);
				this.guesses++;
			}
			return editableWord;
		}
		char x = Character.toUpperCase(c);
		int flag = 0;
		for (int i = 0; i < this.secretWord.length(); i++) {
			if (this.secretWord.charAt(i) == x) {
				this.originalWord[i] = this.dictionaryWord.charAt(i);
				flag = 1;
			}
		}
		if (flag == 1) {
			this.editableWord = String.valueOf(this.originalWord);
			flag = 0;
			return editableWord;
		}
		return null;

	}
	/*
	* Set the maximum number of wrong guesses
	* @param max
	* maximum number of wrong guesses, If is NULL, then assume it 0
	*/
	@Override
	public void setMaxWrongGuesses(final Integer max) {
		if (max == null) {
			this.maxGuesses = 0;
		} else {
			this.maxGuesses = max;
		}
	}
	/*
	 * Get dictionary
	 * @param Rn
	 * @return word
	 */
	public String getDictionary(final int Rn) {
		return dictionary[Rn];
	}
	
}
