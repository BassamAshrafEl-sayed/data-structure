package eg.edu.alexu.csd.datastructure.maze.cs16;

import java.awt.Point;
import java.io.File;
import java.util.Scanner;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import eg.edu.alexu.csd.datastructure.maze.IMazeSolver;

public class Maze implements IMazeSolver {
	private Scanner in;
	public int rows, columns;
	private String[] mazed;
	public boolean found;
	public boolean poped[][];
	public boolean visited[][];
	int[][] result;
	public Navigator bfsEnd;
	StackImp stack = new StackImp();
	LQueue queue = new LQueue();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eg.edu.alexu.csd.datastructure.maze.IMazeSolver#solveBFS(java.io.File)
	 */
	@Override
	public int[][] solveBFS(File maze) {
		mazed = readFile(maze);
		checkValidity();
		findstartbfs();
		while (!queue.isEmpty()) {
			Navigator temp = (Navigator) queue.dequeue();
			visited[temp.index.x][temp.index.y] = true;
			if (mazed[temp.index.x].charAt(temp.index.y) == 'E') {
				found = true;
				// temp.parents.add(temp.index);
				result = new int[temp.parents.size()][2];
				for (int i = 0; i < result.length; i++) {
					Point temp2 = (Point) temp.parents.get(i);
					result[i][0] = temp2.x;
					result[i][1] = temp2.y;
				}
				return result;
			}
			bfs(temp);
		}

		return null;

	}

	/**
	 * 
	 */
	public void findstartbfs() {
		for (int i = 0; i < mazed.length; i++) {
			for (int j = 0; j < mazed[i].length(); j++) {
				if (mazed[i].charAt(j) == 'S') {
					Navigator temp = new Navigator();
					temp.index.x = i;
					temp.index.y = j;
					temp.parents.add(temp.index);
					queue.enqueue(temp);
					visited[i][j] = true;
				}
			}
		}

	}
	public void checkValidity(){
		boolean entry = false , exit = false;
		for(int i = 0;i < mazed.length;i++){
			for(int j = 0;j < mazed[i].length();j++){
				if(mazed[i].charAt(j) == 'S'){
					if(!entry){
						entry = true;
					}else{
						throw null;
					}
				}else if(mazed[i].charAt(j) == 'E'){
					if(!exit){
						exit = true;
					}else{
						throw null;
					}
				}
			}
		}
		if(!entry || !exit){
			throw null;
		}
	}

	/**
	 * 
	 */
	public void bfs(Navigator temp) {

		if (checkLimits(temp.index.x, temp.index.y + 1)) {
			Navigator temp2 = new Navigator();
			if ((mazed[temp.index.x].charAt(temp.index.y + 1) == '.'
					|| mazed[temp.index.x].charAt(temp.index.y + 1) == 'E')
					&& !visited[temp.index.x][temp.index.y + 1]) {
				temp2.index.x = temp.index.x;
				temp2.index.y = temp.index.y + 1;
				setPrevParents(temp, temp2);
				queue.enqueue(temp2);

			}
		}
		if (checkLimits(temp.index.x - 1, temp.index.y)) {
			Navigator temp2 = new Navigator();
			if ((mazed[temp.index.x - 1].charAt(temp.index.y) == '.'
					|| mazed[temp.index.x - 1].charAt(temp.index.y) == 'E')
					&& !visited[temp.index.x - 1][temp.index.y]) {
				temp2.index.x = temp.index.x - 1;
				temp2.index.y = temp.index.y;
				setPrevParents(temp, temp2);
				queue.enqueue(temp2);

			}
		}
		if (checkLimits(temp.index.x, temp.index.y - 1)) {
			Navigator temp2 = new Navigator();
			if ((mazed[temp.index.x].charAt(temp.index.y - 1) == '.'
					|| mazed[temp.index.x].charAt(temp.index.y - 1) == 'E')
					&& !visited[temp.index.x][temp.index.y - 1]) {
				temp2.index.x = temp.index.x;
				temp2.index.y = temp.index.y - 1;
				queue.enqueue(temp2);
				setPrevParents(temp, temp2);
			}
		}
		if (checkLimits(temp.index.x + 1, temp.index.y)) {
			Navigator temp2 = new Navigator();
			if ((mazed[temp.index.x + 1].charAt(temp.index.y) == '.'
					|| mazed[temp.index.x + 1].charAt(temp.index.y) == 'E')
					&& !visited[temp.index.x + 1][temp.index.y]) {
				temp2.index.x = temp.index.x + 1;
				temp2.index.y = temp.index.y;
				queue.enqueue(temp2);
				setPrevParents(temp, temp2);
			}
		}
	}

	void setPrevParents(Navigator x1, Navigator x2) {
		for (int i = 0; i < x1.parents.size(); i++) {
			x2.parents.add(x1.parents.get(i));
		}
		x2.parents.add(x2.index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eg.edu.alexu.csd.datastructure.maze.IMazeSolver#solveDFS(java.io.File)
	 */
	@Override
	public int[][] solveDFS(File maze) {
		mazed = readFile(maze);
		checkValidity();
		findStart(mazed);
		dfs();
		if (stack.size() == 0) {
			return null;
		} else {
			int result[][] = new int[stack.size()][2];
			for (int i = 0; i < result.length; i++) {
				Navigator temp = (Navigator) stack.pop();

				result[result.length - 1 - i][0] = temp.index.x;
				result[result.length - 1 - i][1] = temp.index.y;
			}
			return result;
		}

	}

	/**
	 * @param maze
	 * @return
	 */
	public String[] readFile(File maze) {
		try {
			in = new Scanner(maze);
			rows = Integer.parseInt(in.next());
			columns = Integer.parseInt(in.next());
			String mazeA[] = new String[rows];
			for (int i = 0; i < mazeA.length; i++) {
				mazeA[i] = new String();
			}
			int temp = 0;
			while (in.hasNext()) {
				mazeA[temp++] = in.next();
			}
			poped = new boolean[rows][columns];
			visited = new boolean[rows][columns];
			return mazeA;
		} catch (Exception e) {
			throw null;
		}
	}

	/**
	 * @param x
	 */
	public void findStart(String[] x) {
		for (int i = 0; i < x.length; i++) {
			for (int j = 0; j < x[i].length(); j++) {
				if (x[i].charAt(j) == 's' || x[i].charAt(j) == 'S') {
					Navigator temp = new Navigator();
					temp.index.x = i;
					temp.index.y = j;
					stack.push(temp);
					break;
				}
			}
		}
	}

	/**
	 * 
	 */
	public void dfs() {
		if (!stack.isEmpty() && !found) {
			Navigator temp = (Navigator) stack.peek();
			if (!temp.right) {
				temp.right = true;
				checkPath(temp.index.x, temp.index.y + 1, 1);
			} else if (!temp.up) {
				temp.up = true;
				checkPath(temp.index.x - 1, temp.index.y, 2);
			} else if (!temp.left) {
				temp.left = true;
				checkPath(temp.index.x, temp.index.y - 1, 3);
			} else if (!temp.down) {
				temp.down = true;
				checkPath(temp.index.x + 1, temp.index.y, 4);
			} else {
				Navigator temp2 = (Navigator) stack.pop();

				poped[temp2.index.x][temp2.index.y] = true;
				visited = new boolean[rows][columns];
				dfs();
			}
		}
	}

	/**
	 * @param x
	 * @param y
	 */
	public void checkPath(int x, int y, int direction) {
		if (checkLimits(x, y)) {
			Navigator temp = new Navigator();
			if (mazed[x].charAt(y) == '.') {
				temp.index.x = x;
				temp.index.y = y;
				if (!poped[x][y] && !visited[x][y]) {
					setDirection(temp, direction);
					visited[x][y] = true;
					stack.push(temp);
				}
				dfs();
			} else if (mazed[x].charAt(y) == '#') {
				dfs();
			} else if (mazed[x].charAt(y) == 'E') {
				temp.index.x = x;
				temp.index.y = y;
				stack.push(temp);
				found = true;
			} else if (mazed[x].charAt(y) == 'S') {
				dfs();
			}
		} else {
			dfs();
		}
	}

	/**
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean checkLimits(int x, int y) {
		return (x >= 0) && (x < rows) && (y >= 0) && (y < columns);
	}

	/**
	 * @param x
	 * @param direction
	 */
	public void setDirection(Navigator x, int direction) {
		if (direction == 1) {
			x.left = true;
		} else if (direction == 2) {
			x.down = true;
		} else if (direction == 3) {
			x.right = true;
		} else if (direction == 4) {
			x.up = true;
		}
	}

}
