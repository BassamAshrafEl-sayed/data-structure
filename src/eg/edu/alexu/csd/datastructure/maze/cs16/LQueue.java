package eg.edu.alexu.csd.datastructure.maze.cs16;

import eg.edu.alexu.csd.datastructure.queue.cs16.SinglyLinkedList;

public class LQueue{
	SinglyLinkedList queue = new SinglyLinkedList();
	/**
	* Inserts an item at the queue front.
	*/
	public void enqueue(Object item) {
		queue.add(item);
	}
	/**
	 * Removes the object at the queue rear and returns it.
	 */
	public Object dequeue() {
		Object temp = queue.get(0);
		queue.remove(0);
		return temp;
	}
	/**
	 * Tests if this queue is empty.
	 */
	public boolean isEmpty() {
		return queue.isEmpty();
	}
	/**
	 * Returns the number of elements in the queue
	 */
	public int size() {
		return queue.size();
	}

}
