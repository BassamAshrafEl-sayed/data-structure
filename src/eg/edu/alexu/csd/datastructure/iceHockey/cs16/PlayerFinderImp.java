package eg.edu.alexu.csd.datastructure.iceHockey.cs16;

import java.awt.Point;

import eg.edu.alexu.csd.datastructure.iceHockey.IPlayersFinder;

public class PlayerFinderImp implements IPlayersFinder {
	static int numberOfBoxes = 0;
	static boolean[][] visited;
	public Point[] point = new Point[5000];
	int pointIndexSearch = 0;
	int beginFrom = 0;
	static float quality;
	public Point result[];

	/*
	 * Search for players locations at the given photo
	 * 
	 * @param photo Two dimension array of photo contents Will contain between 1
	 * and 50 elements, inclusive
	 * 
	 * @param team Identifier of the team
	 * 
	 * @param threshold Minimum area for an element Will be between 1 and 10000,
	 * inclusive
	 * 
	 * @return Array of players locations of the given team
	 */
	@Override
	public Point[] findPlayers(final String[] photo, final int team, final int threshold) {
		if (photo == null) {
			return null;
		}
		Point[] points = new Point[50];
		char teamc = String.valueOf(team).charAt(0);
		int pointsIndex = 0;
		visited = new boolean[photo.length][photo[0].length()];
		quality = (float) threshold / 4;
		for (int i = 0; i < photo.length; i++) {
			for (int j = 0; j < photo[i].length(); j++) {
				if (visited[i][j] || teamc != photo[i].charAt(j)) {
					visited[i][j] = true;
					continue;
				}
				beginFrom = pointIndexSearch;
				search(photo, team, threshold, i, j);
				if (numberOfBoxes >= quality && beginFrom < pointIndexSearch) {
					points[pointsIndex] = new Point();
					points[pointsIndex].x = centerX();
					points[pointsIndex++].y = centerY();

				}
				numberOfBoxes = 0;
			}
		}
		result = new Point[pointsIndex];
		for (int i = 0; i < pointsIndex; i++) {
			result[i] = new Point();
			result[i] = points[i];
		}
		sort(result);
		return result;
	}

	/*
	 * Search for players
	 * 
	 * @param photo
	 * 
	 * @param team
	 * 
	 * @param threshold
	 * 
	 * @param row
	 * 
	 * @param column
	 */
	public void search(final String[] photo, final int team, final int threshold, final int row, final int column) {
		char teamc = String.valueOf(team).charAt(0);
		for (int i = row - 1; i <= row + 1; i++) {
			for (int j = column - 1; j <= column + 1; j++) {
				if (i >= 0 && i < photo.length && j >= 0 && j < photo[i].length()) {
					if (visited[i][j] || (i != row && j != column)) {
						continue;
					}
					if (photo[i].charAt(j) == teamc) {
						visited[i][j] = true;
						this.point[this.pointIndexSearch] = new Point();
						this.point[this.pointIndexSearch].x = j;
						this.point[this.pointIndexSearch++].y = i;
						numberOfBoxes++;
						search(photo, team, threshold, i, j);
					}
				}
			}
		}
	}

	/*
	 * Get center x
	 * 
	 * @return Center x
	 */
	public int centerX() {
		int x = 0;
		int xMax;
		int xMin;

		xMax = getMaxX() + 1;
		xMin = getMinX();
		x = xMax + xMin;
		return x;
	}

	/*
	 * Get center y
	 * 
	 * @return Center y
	 */
	public int centerY() {
		int y = 0;
		int yMax;
		int yMin;
		yMax = getMaxY() + 1;
		yMin = getMinY();
		y = yMax + yMin;
		return y;
	}

	/*
	 * Get max x
	 * 
	 * @return max x
	 */
	public int getMaxX() {
		int max = -1;
		for (int i = this.beginFrom; i < this.pointIndexSearch; i++) {
			if (max < this.point[i].x) {
				max = this.point[i].x;
			}
		}
		return max;
	}

	/*
	 * Get max y
	 * 
	 * @return max y
	 */
	public int getMaxY() {
		int max = -1;
		for (int i = this.beginFrom; i < this.pointIndexSearch; i++) {
			if (max < this.point[i].y) {
				max = this.point[i].y;
			}
		}
		return max;
	}

	/*
	 * Get min x
	 * 
	 * @return min x
	 */
	public int getMinX() {
		int min = Integer.MAX_VALUE;
		for (int i = this.beginFrom; i < this.pointIndexSearch; i++) {
			if (min > this.point[i].x) {
				min = this.point[i].x;
			}
		}
		return min;
	}

	/*
	 * Get min y
	 * 
	 * @return min y
	 */
	public int getMinY() {
		int min = Integer.MAX_VALUE;// get the possible maximum value
		for (int i = this.beginFrom; i < this.pointIndexSearch; i++) {
			if (min > this.point[i].y) {
				min = this.point[i].y;
			}
		}
		return min;
	}

	/*
	 * Sort result
	 * 
	 * @param x points
	 */
	public void sort(final Point x[]) {
		Point temp;
		boolean flag = true;
		int noOfmoves = 0;
		while (flag) {
			flag = false;
			for (int i = 1; i < x.length - noOfmoves; i++) {
				if (x[i - 1].x > x[i].x) {
					temp = x[i - 1];
					x[i - 1] = x[i];
					x[i] = temp;
					flag = true;
				} else if (x[i].x == x[i - 1].x && x[i - 1].y > x[i].y) {
					temp = x[i - 1];
					x[i - 1] = x[i];
					x[i] = temp;
					flag = true;
				}
			}
			noOfmoves++;
		}
	}
}
