package eg.edu.alexu.csd.datastructure.calculator.cs16;

import eg.edu.alexu.csd.datastructure.calculator.ICalculator;

public class CalculatorImplementation implements ICalculator {
	/*
	* Adds given two numbers
	* @param x first number
	* @param y second number
	* @return the sum of the two numbers
	*/
	@Override
	public int add(final int x, final int y) {
		return x + y;
	}
	/*
	* Divids two numbers
	* @param x first number
	* @param y second number
	* @return the division result
	*/
	@Override
	public float divide(final int x, final int y) {
		return (float) x / (float) y;
	}

}
