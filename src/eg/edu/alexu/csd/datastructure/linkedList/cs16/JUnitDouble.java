package eg.edu.alexu.csd.datastructure.linkedList.cs16;

import static org.junit.Assert.*;

import org.junit.Test;

public class JUnitDouble {

	@Test
	public void addingNodes() {
		DoublyLinkedList doubly = new DoublyLinkedList();
		doubly.add(1);
		doubly.add(2);
		int actual = (int) doubly.get(0);
		int expected = 1;
		assertEquals(expected, actual);
	}

	@Test
	public void checkInsertion() {
		DoublyLinkedList doubly = new DoublyLinkedList();
		doubly.add(1);
		doubly.add(2);
		doubly.add(3);
		doubly.add(4);

		Object actual = new Object();
		Object expected = new Object();
		for (int i = 0; i < 5; i++) {
			actual = doubly.get(i);
			expected = i + 1;
			if (i == 4) {
				assertNull(actual);
			} else {
				assertEquals(expected, actual);
			}
		}

	}

	@Test
	public void checkAddAtIndex() {
		DoublyLinkedList doubly = new DoublyLinkedList();
		doubly.add(1);
		doubly.add(2);
		doubly.add(3);
		doubly.add(4);
		doubly.add(2, 5);
		Object actual = new Object();
		Object expected = new Object();
		actual = doubly.get(2);
		expected = 5;
		assertEquals(expected, actual);

	}

	@Test
	public void checkSetting() {
		DoublyLinkedList doubly = new DoublyLinkedList();
		doubly.add(1);
		doubly.add(2);
		doubly.add(3);
		doubly.add(4);
		doubly.set(0, 10);
		Object actual = new Object();
		Object expected = new Object();
		actual = doubly.get(0);
		expected = 10;
		assertEquals(expected, actual);

	}

	@Test
	public void checkSublist() {
		DoublyLinkedList doubly = new DoublyLinkedList();
		for (int i = 0; i <= 10; i++) {
			doubly.add(i);
		}
		DoublyLinkedList actual = new DoublyLinkedList();
		DoublyLinkedList expected = new DoublyLinkedList();
		actual = (DoublyLinkedList) doubly.sublist(2, 6);
		for (int i = 2; i <= 6; i++) {
			expected.add(i);
		}
		for (int i = 0; i < 6; i++) {
			assertEquals(expected.get(i), actual.get(i));
		}

	}
	@Test
	public void checkRemove() {
		DoublyLinkedList doubly = new DoublyLinkedList();
		doubly.add(1);
		doubly.add(2);
		doubly.add(3);
		doubly.add(4);

		int expected = doubly.size() - 1;
		int actual;
		doubly.remove(3);
		actual = doubly.size();

		assertEquals(expected, actual);

	}
	@Test
	public void checkContains() {
		DoublyLinkedList doubly = new DoublyLinkedList();
		for (int i = 0; i <= 10; i++) {
			doubly.add(i);
		}
		boolean actual = doubly.contains(10);
		boolean expected = true;
		assertEquals(expected, actual);
		actual = doubly.contains(11);
		expected = false;
		assertEquals(expected, actual);
	}
	@Test
	public void checkClear() {
		DoublyLinkedList doubly = new DoublyLinkedList();
		for (int i = 0; i <= 10; i++) {
			doubly.add(i);
		}
		doubly.clear();
		boolean actual = doubly.isEmpty();
		boolean expected = true;
		assertEquals(expected, actual);
		doubly.add(0);
		actual = doubly.isEmpty();
		expected = false;
		assertEquals(expected, actual);
	}


}
