package eg.edu.alexu.csd.datastructure.linkedList.cs16;

import static org.junit.Assert.*;

import org.junit.Test;

public class JUnitSingle {

	@Test
	public void addingNodes() {
		SinglyLinkedList single = new SinglyLinkedList();
		single.add(1);
		single.add(2);
		int actual = (int) single.get(0);
		int expected = 1;
		assertEquals(expected, actual);
	}

	@Test
	public void checkInsertion() {
		SinglyLinkedList single = new SinglyLinkedList();
		single.add(1);
		single.add(2);
		single.add(3);
		single.add(4);

		Object actual = new Object();
		Object expected = new Object();
		for (int i = 0; i < 5; i++) {
			actual = single.get(i);
			expected = i + 1;
			if (i == 4) {
				assertNull(actual);
			} else {
				assertEquals(expected, actual);
			}
		}

	}

	@Test
	public void checkAddAtIndex() {
		SinglyLinkedList single = new SinglyLinkedList();
		single.add(1);
		single.add(2);
		single.add(3);
		single.add(4);
		single.add(2, 5);
		Object actual = new Object();
		Object expected = new Object();
		actual = single.get(2);
		expected = 5;
		assertEquals(expected, actual);

	}

	@Test
	public void checkSetting() {
		SinglyLinkedList single = new SinglyLinkedList();
		single.add(1);
		single.add(2);
		single.add(3);
		single.add(4);
		single.set(0, 10);
		Object actual = new Object();
		Object expected = new Object();
		actual = single.get(0);
		expected = 10;
		assertEquals(expected, actual);

	}

	@Test
	public void checkSublist() {
		SinglyLinkedList single = new SinglyLinkedList();
		for (int i = 0; i <= 10; i++) {
			single.add(i);
		}
		SinglyLinkedList actual = new SinglyLinkedList();
		SinglyLinkedList expected = new SinglyLinkedList();
		actual = (SinglyLinkedList) single.sublist(2, 6);
		for (int i = 2; i <= 6; i++) {
			expected.add(i);
		}
		for (int i = 0; i < 6; i++) {
			assertEquals(expected.get(i), actual.get(i));
		}

	}
	@Test
	public void checkRemove() {
		SinglyLinkedList single = new SinglyLinkedList();
		single.add(1);
		single.add(2);
		single.add(3);
		single.add(4);

		int expected = single.size() - 1;
		int actual;
		single.remove(3);
		actual = single.size();

		assertEquals(expected, actual);

	}
	@Test
	public void checkContains() {
		SinglyLinkedList single = new SinglyLinkedList();
		for (int i = 0; i <= 10; i++) {
			single.add(i);
		}
		boolean actual = single.contains(10);
		boolean expected = true;
		assertEquals(expected, actual);
		actual = single.contains(11);
		expected = false;
		assertEquals(expected, actual);
	}
	@Test
	public void checkClear() {
		SinglyLinkedList single = new SinglyLinkedList();
		for (int i = 0; i <= 10; i++) {
			single.add(i);
		}
		single.clear();
		boolean actual = single.isEmpty();
		boolean expected = true;
		assertEquals(expected, actual);
		single.add(0);
		actual = single.isEmpty();
		expected = false;
		assertEquals(expected, actual);
	}

}
