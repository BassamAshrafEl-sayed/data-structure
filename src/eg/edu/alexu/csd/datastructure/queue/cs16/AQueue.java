package eg.edu.alexu.csd.datastructure.queue.cs16;

import eg.edu.alexu.csd.datastructure.queue.IArrayBased;
import eg.edu.alexu.csd.datastructure.queue.IQueue;

public class AQueue implements IQueue, IArrayBased {

	public int front = -1;
	public int last = -1;
	public int n;
	public int size = 0;
	public Object queue[];

	AQueue(int n) {
		if (n <= 0) {
			throw null;
		} else {
			queue = new Object[n];
			this.n = n;
		}
	}
	/**
	 * Inserts an item at the queue front.
	 */
	@Override
	public void enqueue(Object item) {
		if (size >= n) {
			throw null;
		} else {
			size++;
			last = (last + 1) % n;
			queue[last] = item;
		}
	}

	/**
	 * Removes the object at the queue rear and returns it.
	 */
	@Override
	public Object dequeue() {
		if(size == 0){
			front = -1;
			last = -1;
			throw null;
		}else{
			front = (front+1)%n;
			size--;
			return queue[front];
			 
		}
	}

	/**
	 * Tests if this queue is empty.
	 */
	@Override
	public boolean isEmpty() {
		if(size == 0)
			return true;
		return false;
	}

	/**
	 * Returns the number of elements in the queue
	 */
	@Override
	public int size() {
		return size;
	}
}
