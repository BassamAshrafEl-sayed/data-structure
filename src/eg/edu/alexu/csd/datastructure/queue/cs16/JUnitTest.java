package eg.edu.alexu.csd.datastructure.queue.cs16;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class JUnitTest {

	@Test
	public void TestFull() {
		boolean actual = false;
		AQueue aqueue = new AQueue(5);
		for (int i = 0; i < 6; i++) {
			try {
				aqueue.enqueue(0);
			} catch (Exception e) {
				actual = true;
			}
		}
		boolean expected = true;
		assertEquals(expected, actual);
	}

	@Test
	public void TestEmptyAQ() {
		AQueue aqueue = new AQueue(5);
		for (int i = 0; i < 5; i++) {
			aqueue.enqueue(0);
		}
		for (int i = 0; i < 5; i++) {
			aqueue.dequeue();
		}
		boolean actual = aqueue.isEmpty();
		boolean expected = true;
		assertEquals(expected, actual);
	}

	@Test
	public void TestEmptyLQ() {
		LQueue lqueue = new LQueue();
		for (int i = 0; i < 5; i++) {
			lqueue.enqueue(0);
		}
		for (int i = 0; i < 5; i++) {
			lqueue.dequeue();
		}
		boolean actual = lqueue.isEmpty();
		boolean expected = true;
		assertEquals(expected, actual);
	}

	@Test
	public void TestDequeEmptyAQ() {
		AQueue aqueue = new AQueue(6);
		boolean actual = false;
		for (int i = 0; i < 5; i++) {
			aqueue.enqueue(0);
		}
		for (int i = 0; i < 6; i++) {
			try {
				aqueue.dequeue();
			} catch (Exception e) {
				actual = true;
			}
		}
		boolean expected = true;
		assertEquals(expected, actual);
	}

	@Test
	public void TestDequeEmptyLQ() {
		LQueue lqueue = new LQueue();
		boolean actual = false;
		for (int i = 0; i < 5; i++) {
			lqueue.enqueue(0);
		}
		for (int i = 0; i < 6; i++) {
			try {
				lqueue.dequeue();
			} catch (Exception e) {
				actual = true;
			}
		}
		boolean expected = true;
		assertEquals(expected, actual);
	}

	@Test
	public void TestSizeAQ_LQ() {
		AQueue aqueue = new AQueue(5);
		LQueue lqueue = new LQueue();
		// =======Array=====
		for (int i = 0; i < 3; i++) {
			aqueue.enqueue(0);
		}
		for (int i = 0; i < 1; i++) {
			aqueue.dequeue();

		}
		int actual = aqueue.size();
		int expected = 2;
		assertEquals(expected, actual);
		// ========linked======
		for (int i = 0; i < 6; i++) {
			lqueue.enqueue(0);
		}
		for (int i = 0; i < 2; i++) {
			lqueue.dequeue();

		}
		actual = lqueue.size();
		expected = 4;
		assertEquals(expected, actual);
	}

	@Test
	public void TestCircular() {
		AQueue aqueue = new AQueue(10);

		for (int i = 0; i < 10; i++) {
			aqueue.enqueue(i);//size 10
		}
		for (int i = 0; i < 5; i++) {
			aqueue.dequeue();//size 5

		}
		for (int i = 0; i < 3; i++) {
			aqueue.enqueue(i);//size 8
		}
		int actual = (int) aqueue.dequeue();//size 7;
		int expected = 5;
		assertEquals(expected, actual);
		actual = aqueue.size();
		expected = 7;
		assertEquals(expected, actual);
		
	}
	@Test
	public void TestDiffernetObject() {
		LQueue lqueue = new LQueue();
		lqueue.enqueue(1);
		lqueue.enqueue('b');
		lqueue.enqueue("Bassam");
		lqueue.enqueue(1.5f);
		Object actual = lqueue.dequeue();
		Object expected = 1;
		assertEquals(expected, actual);
		actual = lqueue.dequeue();
		expected = 'b';
		assertEquals(expected, actual);
		actual = lqueue.dequeue();
		expected = "Bassam";
		assertEquals(expected, actual);
		actual = lqueue.dequeue();
		expected = 1.5f;
		assertEquals(expected, actual);
	}
	@Test
	public void TestLong() {
		LQueue lqueue = new LQueue();
		for(int i=0;i<10000;i++){
			lqueue.enqueue(i);
		}
		int actual = lqueue.size();
		int expected = 10000;
		assertEquals(expected, actual);
	}

}
