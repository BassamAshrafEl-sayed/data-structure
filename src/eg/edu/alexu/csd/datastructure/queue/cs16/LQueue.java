package eg.edu.alexu.csd.datastructure.queue.cs16;

import eg.edu.alexu.csd.datastructure.queue.ILinkedBased;
import eg.edu.alexu.csd.datastructure.queue.IQueue;

public class LQueue implements IQueue, ILinkedBased {
	SinglyLinkedList queue = new SinglyLinkedList();
	/**
	* Inserts an item at the queue front.
	*/
	@Override
	public void enqueue(Object item) {
		queue.add(item);
	}
	/**
	 * Removes the object at the queue rear and returns it.
	 */
	@Override
	public Object dequeue() {
		Object temp = queue.get(0);
		queue.remove(0);
		return temp;
	}
	/**
	 * Tests if this queue is empty.
	 */
	@Override
	public boolean isEmpty() {
		return queue.isEmpty();
	}
	/**
	 * Returns the number of elements in the queue
	 */
	@Override
	public int size() {
		return queue.size();
	}

}
