package eg.edu.alexu.csd.datastructure.queue.cs16;

public class Node {
	public Object value;
	public Node next;
	public Node prev;
}
